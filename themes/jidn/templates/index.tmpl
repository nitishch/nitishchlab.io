## -*- coding: utf-8 -*-
<%namespace name="helper" file="index_helper.tmpl"/>
<%namespace name="comments" file="comments_helper.tmpl"/>
<%inherit file="base.tmpl"/>

<%block name="extra_head">
    ${parent.extra_head()}
    % if posts and (permalink == '/' or permalink == '/' + index_file):
        <link rel="prefetch" href="${posts[0].permalink()}" type="text/html">
    % endif
</%block>

<%block name="content">
<%block name="content_header"></%block>
<div class="posts">
% for post in posts:
    <article class="post h-entry post-${post.meta('type')}">
    <header>
        <h1 class="post-title p-name"><a href="${post.permalink()}" class="u-url">${post.title()|h}</a></h1>
        <div class="metadata">
            <p class="dateline"><a href="${post.permalink()}" rel="bookmark"><time class="post-date published dt-published" datetime="${post.date.isoformat()}" title="${post.formatted_date(date_format)}">${post.formatted_date(date_format)}</time></a></p>
            % if not post.meta('nocomments') and site_has_comments:
                <p class="commentline">${comments.comment_link(post.permalink(), post._base_path)}
            % endif
        </div>
    </header>
    %if index_teasers:
    <div class="p-summary entry-summary">
    ${post.text(teaser_only=True)}
    %else:
    <div class="e-content entry-content">
    ${post.text(teaser_only=False)}
    %endif
    </div>
    </article>
% endfor
</div>
${helper.html_pager()}
${comments.comment_link_script()}
${helper.mathjax_script(posts)}
## This appears at the end of the content
<%block name="bio">
<div itemprop="author" itemscope itemtype="http://schema.org/Person">
%if blog_author in JIDN:
<footer class="byline author author-bio vcard">
    <hr/>
    % if 'image' in JIDN[blog_author]:
         <meta itemprop="image" content="${JIDN[blog_author]['image']}"/>
         <div class="author-image" style="background: url(${JIDN[blog_author]['image']})" />
    % endif
     <h3 class="byline-name fn" itemprop="name">${blog_author|trim}
     % if 'email' in JIDN[blog_author]:
        <span class='post-sharing'>
            <a href="mailto:${JIDN[blog_author]['email']}"
            aria-label="Email author">
            <i class="fa fa-2 fa-fw fa-envelope" aria-hidden="true" title="Email author" /i>
            </a>
        </span>
     % endif
     % if 'social' in JIDN[blog_author]:
        <span class='post-sharing'>
        %for url in JIDN[blog_author]['social']:
            ${url|social_link}
        %endfor
        </span>
     % endif
     </h3>
     % if 'bio' in JIDN[blog_author]:
     <p class="bio">${JIDN[blog_author]['bio']}</p>
     % endif
     % if 'map' in JIDN[blog_author]:
     <p><i class="fa fa-map-marker"/> ${JIDN[blog_author]['map']}</p>
     % endif
</footer>
</div>
%endif
</%block>
</%block>


<%!
def social_link(url):
    try:
        from urlparse import urlparse
    except ImportError:
        from urllib.parse import urlparse

    o = urlparse(url)
    site = o.netloc.lower()
    substitute = dict((('Github', 'GitHub'),
                     ('Stackoverflow', 'StackOverflow'),
                     ('stackoverflow', 'stack-overflow'),
                     ('Youtube', 'YouTube'),
                     ('Linkedin', 'LinkedIn'),
                     ('Whatsapp', 'WhatsApp'),
                     ('Bizsugar', 'BizSugar'),
                     ))
    if site == "plus.google.com":
        icon = "google-plus"
        title = "Google+"
    else:
        try:
            icon = o.netloc.split('.')[-2]
        except Exception as err:
            print(err)
            raise err
            return ''
        title = substitute.get(icon.title(), icon.title())
        icon = substitute.get(icon, icon)

    rv = '''<a href="{url}" target="_blank" arial-label="Go to {title}"> <i class="fa fa-fw fa-{icon}" aria-hidden="true" title="{title}" /></a>'''.format(url=url, icon=icon, title=title)
    return rv
%>
